<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Equipo $model */

$this->title = 'Update Equipo: ' . $model->nomequipo;
$this->params['breadcrumbs'][] = ['label' => 'Equipos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nomequipo, 'url' => ['view', 'nomequipo' => $model->nomequipo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="equipo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
